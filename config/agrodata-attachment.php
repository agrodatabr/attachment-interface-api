<?php

return [
    'url' => env('AGRODATA_ATTACHMENT_URL'),
    'key' => env('AWS_ACCESS_KEY_ID', config('filesystems.disks.s3.key')),
    'secret' => env('AWS_SECRET_ACCESS_KEY', config('filesystems.disks.s3.secret')),
    'token' => env('AWS_SESSION_TOKEN', config('filesystems.disks.s3.token', null)),
    'region' => env('AWS_DEFAULT_REGION', config('filesystems.disks.s3.region')),
    'bucket' => env('AWS_BUCKET', config('filesystems.disks.s3.bucket'))
];
