<?php

namespace Agrodata\Attachment;

use GuzzleHttp\Client;

abstract class HttpService
{
    protected string $url;
    protected array $authorization;

    public function __construct(string $apiUrl = null)
    {
        $this->url = $apiUrl ?? config('agrodata-attachment.url');
        $this->authorization = [
            'key'    => config('agrodata-attachment.key'),
            'secret' => config('agrodata-attachment.secret'),
            'region' => config('agrodata-attachment.region'),
            'bucket' => config('agrodata-attachment.bucket'),
            'token'  => config('agrodata-attachment.token'),
        ];
    }

    protected function callApi(string $method, string $path, array $params, string $paramType)
    {
        return (new Client())
            ->$method("{$this->url}{$path}", [ $paramType => [...$params, ...$this->authorization] ])
            ->getBody()
            ->getContents();
    }

    protected function post(string $path, array $params = [])
    {
        return $this->callApi('post', $path, $params, 'form_params');
    }

    protected function delete(string $path, array $params = [])
    {
        return $this->callApi('delete', $path, $params, 'form_params');
    }

    protected function get(string $path, array $params = [])
    {
        return $this->callApi('get', $path, $params, 'query');
    }
}