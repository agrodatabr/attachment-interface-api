<?php

namespace Agrodata\Attachment;

use GuzzleHttp\Client;

class AttachmentService extends HttpService
{
    public function presignUpload(string $fileName): array
    {
        return json_decode($this->post('/presign-upload', [ 'filename' => $fileName ]), true);
    }

    public function presignPost(string $fileName): array
    {
        return json_decode($this->post('/presign-post', [ 'filename' => $fileName ]), true);
    }

    public function presignDownload(string $fileName, bool $forceDownload = false): string
    {
        return $this->get('/presign-download', [ 'filename' => $fileName, 'force-download' => $forceDownload ]);
    }

    public function presignDownloadShort(string $fileName, bool $forceDownload = false): string
    {
        return $this->get('/presign-download-short', [ 'filename' => $fileName, 'force-download' => $forceDownload]);
    }

    public function queryDownload(string $fileName, string $query): string
    {
        return $this->get('/query-download', [ 'filename' => $fileName, 'query' => $query ]);
    }

    public function downloadZipper(array $fileNames): string
    {
        return $this->post('/zipper', [ 'files' => $fileNames ]);
    }

    public function deleteAttachment(string $fileName): string
    {
        return $this->delete('/delete', [ 'filename' => $fileName ]);
    }

    public function downloadPDFWatermarked(string $fileName, string $text, array $options = []): string
    {
        return $this->get('/pdf-watermarked', [ 'filename' => $fileName, 'text' => $text, 'options' => $options ]);
    }
}
