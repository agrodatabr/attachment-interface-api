<?php

namespace Agrodata\Attachment\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class InstallAttachmentConsole extends Command
{
    protected $signature = 'agrodata-attachment:install';
    protected $description = 'Install the Agrodata Attachment Service';

    public function handle()
    {
        $this->info('Installing Attachment Service...');
        $this->info('Publishing configuration...');

        if (! $this->configExists('agrodata-attachment.php')) {
            $this->publishConfiguration();
            $this->info('Published configuration');
        } else {
            if ($this->shouldOverwriteConfig()) {
                $this->info('Overwriting configuration file...');
                $this->publishConfiguration($force = true);
            } else {
                $this->info('Existing configuration was not overwritten');
            }
        }

        $this->info('Installed BlogPackage');
    }

    private function configExists($fileName)
    {
        return File::exists(config_path($fileName));
    }

    private function shouldOverwriteConfig()
    {
        return $this->confirm(
            'Config file already exists. Do you want to overwrite it?',
            false
        );
    }

    private function publishConfiguration($forcePublish = false)
    {
        $params = [
            '--provider' => "Agrodata\Attachment\AttachmentServiceProvider",
            '--tag' => "config"
        ];

        if ($forcePublish === true) {
            $params['--force'] = true;
        }

        $this->call('vendor:publish', $params);
    }
}