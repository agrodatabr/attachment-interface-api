<p align="center">
    <img src="attachment.png" alt="logo"/>
</p>
<p align="center">
    &nbsp;<img src="https://img.shields.io/packagist/v/agrodata/attachment.svg" />
     <img src="https://img.shields.io/packagist/dt/agrodata/attachment.svg" />
</p>

## Attachment Service
Pacote responsável por configurar e utilizar o microserviço de anexos do agrodata nos projetos laravel
. Em resumo, o microserviço é responsável por toda manipulação de dados do S3 nos projetos
a fim de centralizar o alto processamento demandado (memória e processador) em apenas um lugar
Além de possibilitar operações mais pesadas, como baixar diversos arquivos simultaneos no formato .zip e 
realizar upload de arquivo grandes. 

Para saber mais, acesse a documentação do microserviço:
* https://bitbucket.org/agrodatabr/attachment-service-api

A função deste pacote é apenas um "meio campo" entre os projetos laravel e o microserviço, pois, ele usará os acessos
de conexão do S3 do projeto, enviará para o microserviço e receberá o resultado;


### Instalação
#### Laravel / Lumen
Instale o pacote através do composer com o seguinte comando:

```
composer require agrodata/attachment-service
```

#### *Apenas para "Lumen framework"
Registre o arquivo de provider em ```bootstrap/app.php```
adicionando a seguinte linha no final do arquivo.

```php
$app->register(\Agrodata\AttachmentServiceProvider::class);
```

### Configuração
Para configurar a biblioteca utilize o seguinte comando, que irá registrar o provider e criar o arquivo de configuração (opcional) dentro do projeto.
Em resumo, o arquivo de configuração apenas procura os dados de Storage/S3 no projeto, mas, o parametro com a URL da api de microserviço de anexos (```AGRODATA_ATTACHMENT_URL```) é obrigatório.

```
php artisan agrodata-attachment:install
```


```php
<?php # config/agrodata-attachment.php

return [
    'url' => env('AGRODATA_ATTACHMENT_URL'),
    'key' => env('AWS_ACCESS_KEY_ID', config('filesystems.disks.s3.key')),
    'secret' => env('AWS_SECRET_ACCESS_KEY', config('filesystems.disks.s3.secret')),
    'region' => env('AWS_DEFAULT_REGION', config('filesystems.disks.s3.region')),
];
```

### Utilização

Após configurado, a classe \Agrodata\Attachment\AttachmentService::class,
poderá ser instanciada em qualquer lugar do projeto, podendo assim, chamar os métodos 
de manipulação do microserviço de anexos;

Os métodos de download sempre retornarão links que possibilitarão o download dos arquivos. ```Todos os links gerados pelo serviço terão validade de 5 minutos```

#### downloadZipper(array $filenames): string
Receberá uma lista de arquivos (devem existir no S3, senão, gerará erro), criará um arquivo com extensão .zip e a URL do arquivo zipado para download.

Retornará o link de download do zip.
```php
<?php

use Agrodata\Attachment\AttachmentService;

$downloadUrl = (new AttachmentService)->downloadZipper(["lista-compras.txt", "fornecedores.xlsx"]);
```

#### presignDownload(string $filename): string
Recebe o nome de um arquivo (existente no S3) e retornar o link de download.
```php
<?php

use Agrodata\Attachment\AttachmentService;

$downloadUrl = (new AttachmentService)->presignDownload("lista-compras.txt");
```

#### presignDownloadShort(string $filename): string
Recebe o nome de um arquivo (existente no S3) e retornar o link de download com a URL encurtada. formato:
`short.service.agrodata.agr.br/xxxxxx`.
```php
<?php

use Agrodata\Attachment\AttachmentService;

$downloadUrl = (new AttachmentService)->presignDownloadShort("lista-compras.txt");
```

#### queryDownload(string $filename, string $query): string
Recebe o nome de uma fonte de dados JSON (existente no S3) e uma query para pesquisa. Retorna um "chunk" do JSON filtrado.
```php
<?php

use Agrodata\Attachment\AttachmentService;

$jsonReturn = (new AttachmentService)->queryDownload("fonteDeDados.json", "select * from s3Object");
```

#### deleteAttachment(string $filename): string
Recebe o nome de um arquivo (existente no S3) e realiza a deleção.
```php
<?php

use Agrodata\Attachment\AttachmentService;

$downloadUrl = (new AttachmentService)->deleteAttachment("lista-compras.txt");
```

#### presignUpload(string $filename): string
Recebe o nome do arquivo que será salvo no S3, retornará um link de upload. Para esse link recebido, deverá ser enviado o parâmetro ```file``` via ```POST```, para realizar efetivamente o anexo do 
arquivo.
Dessa maneira, o upload pode ser feito direto pelo frontend usando a API do S3, respeitando apenas as limitações e configurações do bucket.
```php
<?php

use Agrodata\Attachment\AttachmentService;
use Illuminate\Support\Facades\Http;

$uploadUrl = (new AttachmentService)->presignUpload("lista-compras.txt");

$fileSaved = Http::asForm()->put($uploadUrl, ['file' => ...binaryContent])->ok();
```

#### presignPost(string $filename): array
Recebe o nome do arquivo que será salvo no S3, retornará um array com os dados de formulário que serão necessários para o POST. Além dos dados de form-input, será necessário incluir o ```file```.
Dessa maneira, o upload pode ser feito direto pelo frontend usando a API do S3, respeitando apenas as limitações e configurações do bucket.
```php
<?php

use Agrodata\Attachment\AttachmentService;
use Illuminate\Support\Facades\Http;

$requestData = (new AttachmentService)->presignPost("lista-compras.txt");

$fileSaved = Http::asForm()->post($requestData['attributes']['action'], ['file' => ...binaryContent])->ok();
```

#### downloadPDFWatermarked(string $filename, string $text, array $options = []): string
Recebe o nome do arquivo pdf que está salvo no S3, o texto para inserção na marca dágua e algums opções adicionais de tamanho, cor e angulo. Retornará um link de download. Método GET.
```php
<?php

use Agrodata\Attachment\AttachmentService;
use Illuminate\Support\Facades\Http;

$downloadURL = (new AttachmentService)->downloadPDFWatermarked(
    "relatorio.pdf", //filename
    "Rascunho", //watermark text
    [ //options
        "size" => '150' //optional, default: 200
        "angle" => '90', //optional, default: 90
        "color" => 'lightgray' //optional, default: #CC00007F
    ]
);
```

### Fluxo de utilização de anexos completa
<p align="center">
    <img src="flow.png" alt="logo"/>
</p>